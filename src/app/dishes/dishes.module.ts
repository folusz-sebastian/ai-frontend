import {NgModule} from '@angular/core';
import {DishesComponent} from './dishes.component';
import {DishComponent} from './dishes-list/dish/dish.component';
import {DishesListComponent} from './dishes-list/dishes-list.component';
import {DishInfoComponent} from './dish-info/dish-info.component';
import {CommonModule} from '@angular/common';
import {DishesRoutingModule} from './dishes-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MatSidenavModule} from '@angular/material';
import {DishService} from '../service/dish.service';
import { DishEditingComponent } from './dish-info/dish-editing/dish-editing.component';
import { DishInfoDisplayingComponent } from './dish-info/dish-info-displaying/dish-info-displaying.component';
import {CurrencyMaskModule} from 'ng2-currency-mask';

@NgModule({
  declarations: [
    DishesComponent,
    DishesListComponent,
    DishComponent,
    DishInfoComponent,
    DishEditingComponent,
    DishInfoDisplayingComponent
  ],
  imports: [
    CommonModule,
    DishesRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatSidenavModule,
    CurrencyMaskModule
  ],
  providers: [
    DishService
  ]
})
export class DishesModule {

}
