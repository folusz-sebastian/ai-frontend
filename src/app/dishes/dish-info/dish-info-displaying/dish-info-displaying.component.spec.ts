import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DishInfoDisplayingComponent } from './dish-info-displaying.component';

describe('DishInfoDisplayingComponent', () => {
  let component: DishInfoDisplayingComponent;
  let fixture: ComponentFixture<DishInfoDisplayingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DishInfoDisplayingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DishInfoDisplayingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
