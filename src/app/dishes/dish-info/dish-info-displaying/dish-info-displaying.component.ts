import {Component, Input, OnInit} from '@angular/core';
import {Dish} from '../../../model/dish/dish.model';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {DishService} from '../../../service/dish.service';

@Component({
  selector: 'app-dish-info-displaying',
  templateUrl: './dish-info-displaying.component.html',
  styleUrls: ['./dish-info-displaying.component.scss']
})
export class DishInfoDisplayingComponent implements OnInit {
  @Input() dish: Dish;

  constructor() { }

  ngOnInit() {}

}
