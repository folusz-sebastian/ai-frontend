import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Dish} from '../../../model/dish/dish.model';
import {DishCategory} from '../../../model/dish/dish-category.model';
import {DishService} from '../../../service/dish.service';
import {NgForm} from '@angular/forms';
import {Subscription} from 'rxjs';
import {JsonConvert, ValueCheckingMode} from 'json2typescript';
import {Ingredient} from '../../../model/dish/ingredient.model';
import {DishIngredient} from '../../../model/dish/dish-ingredient.model';
import {StatusBarService} from '../../../service/status-bar.service';

@Component({
  selector: 'app-dish-editing',
  templateUrl: './dish-editing.component.html',
  styleUrls: ['./dish-editing.component.scss']
})
export class DishEditingComponent implements OnInit, AfterViewInit, OnDestroy {
  private static dishWasSavedMessage = 'Nowe danie zostało zapisane';
  private static dishWasUpdatedMessage = 'Danie zostało zaktualizowane';
  @ViewChild('form', {static: false}) form: NgForm;
  @Input() dish: Dish;
  dishTemp: Dish;
  @Input() dishesArrayIndex: number;
  @Input() newDish: boolean;
  @Input() editableDish: boolean;
  dishCategories: DishCategory[];
  ingredients: Ingredient[];
  formChangesSubscription: Subscription;
  private dishWasSavedTimerSubscription: Subscription;
  private dishWasUpdatedTimerSubscription: Subscription;

  constructor(private dishService: DishService,
              private statusBarService: StatusBarService) {
  }

  ngOnInit() {
    const jsonConvert = new JsonConvert();
    jsonConvert.ignorePrimitiveChecks = false;
    jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL;

    this.dishTemp = jsonConvert.deserializeObject(jsonConvert.serializeObject(this.dish), Dish);
    this.dishService.retrieveDishCategories().subscribe(
      (dishCategories: DishCategory[]) => {
        this.dishCategories = dishCategories;
      }
    );
    this.dishService.retrieveIngredients().subscribe(
      (ingredients: Ingredient[]) => {
        this.ingredients = ingredients;
      }
    );
  }

  ngAfterViewInit() {
    this.formChangesSubscription = this.form.form.valueChanges.subscribe(
      () => {
        this.dishService.ingredientFormValid.next(this.form);
      }
    );
  }

  saveForm() {
    console.log('zapisuje');
    this.dishTemp.category.id = +this.dishTemp.category.id;
    this.dishTemp.dishIngredients.forEach(dishIngredient => dishIngredient.ingredient.id = +dishIngredient.ingredient.id);

    this.editableDish = false;
    this.dishService.editButtonPressed.next(this.editableDish);
    if (this.newDish) {
      this.dishService.postNewDish(this.dishTemp).subscribe(
        (dish) => {
          console.log(dish);
          this.dishWasSavedTimerSubscription = this.statusBarService.temporarilyShowStatusBar(DishEditingComponent.dishWasSavedMessage);
          this.dishService.saveNewDish(dish);
        }
      );
    } else {
      this.dishService.putDish(this.dishTemp).subscribe(
        (dish: Dish) => {
          console.log(dish);
          this.dishService.updateDish(this.dishesArrayIndex, dish);
          this.dishWasUpdatedTimerSubscription = this.statusBarService.temporarilyShowStatusBar(DishEditingComponent.dishWasUpdatedMessage);
        }
      );
    }
  }

  addIngredient() {
    this.dishTemp.dishIngredients.push(new DishIngredient());
    console.log('po dodaniu skladnika skladniki: ' + JSON.stringify(this.dishTemp.dishIngredients));
  }

  removeIngredient(ingredientIndex: number) {
    if (ingredientIndex > -1 && ingredientIndex < this.dishTemp.dishIngredients.length) {
      this.dishTemp.dishIngredients.splice(ingredientIndex, 1);
      console.log('warunek spelniony ingredientIndex:' + ingredientIndex);
    }
    console.log('po usunieciu skladnika skladniki: ' + JSON.stringify(this.dishTemp.dishIngredients));
  }

  ingredientFromListById(ingredientId: number): Ingredient {
    return this.ingredients
      .find(
        ingredient => ingredient.id === Number(ingredientId)
      );
  }

  getMeasurementUnitName(ingredientId: number): string {
    if (ingredientId === null) {
      return '';
    }
    return this.ingredientFromListById(ingredientId).measurementUnit.name;
  }

  ngOnDestroy(): void {
    this.formChangesSubscription.unsubscribe();
    if (this.dishWasSavedTimerSubscription && this.dishWasSavedTimerSubscription instanceof Subscription) {
      this.dishWasSavedTimerSubscription.unsubscribe();
    }
    if (this.dishWasUpdatedTimerSubscription && this.dishWasUpdatedTimerSubscription instanceof Subscription) {
      this.dishWasUpdatedTimerSubscription.unsubscribe();
    }
  }

}
