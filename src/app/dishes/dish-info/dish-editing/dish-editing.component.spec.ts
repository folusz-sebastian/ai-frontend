import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DishEditingComponent } from './dish-editing.component';

describe('DishEditingComponent', () => {
  let component: DishEditingComponent;
  let fixture: ComponentFixture<DishEditingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DishEditingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DishEditingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
