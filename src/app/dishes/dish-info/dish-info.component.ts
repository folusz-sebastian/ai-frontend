import {Component, OnDestroy, OnInit} from '@angular/core';
import {Dish} from '../../model/dish/dish.model';
import {ActivatedRoute, Params} from '@angular/router';
import {DishService} from '../../service/dish.service';
import {Subscription} from 'rxjs';
import {StatusBarService} from '../../service/status-bar.service';

@Component({
  selector: 'app-dish-info',
  templateUrl: './dish-info.component.html',
  styleUrls: ['./dish-info.component.scss']
})
export class DishInfoComponent implements OnInit, OnDestroy {
  private static removingDishMessage = 'Danie zostało usunięte';
  dish: Dish;
  editableDish: boolean;
  newDish: boolean;
  dishesArrayIndex: number;
  private editBtnSubscription: Subscription;
  private removeBtnSubscription: Subscription;
  private timerSubscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute,
              private dishService: DishService,
              private statusBarService: StatusBarService) {
  }

  ngOnInit() {
    console.log('jestem w dish info');
    this.editableDish = false;
    this.newDish = false;
    this.activatedRoute.params.subscribe(
      (params: Params) => {
        if (params.id === undefined) {
          this.newDish = true;
        }

        if (this.newDish) {
          this.dish = new Dish();
          this.editableDish = true;
          this.dishService.editButtonPressed.next(this.editableDish);
        } else {
          this.dishesArrayIndex = +params.id;
          this.dish = this.dishService.getDish(this.dishesArrayIndex);
        }
      }
    );

    this.editBtnSubscription = this.dishService.editButtonPressed.subscribe(
      (editButtonPressed: boolean) => {
        this.editableDish = editButtonPressed;
        console.log('editableDish: ' + editButtonPressed);
      }
    );

    this.removeBtnSubscription = this.dishService.removeButtonPressed.subscribe(
      () => {
        console.log('usuwam danie');
        return this.dishService.removeDish(this.dishesArrayIndex).subscribe(
          (response) => {
            this.timerSubscription = this.statusBarService.temporarilyShowStatusBar(DishInfoComponent.removingDishMessage);
            console.log(response);
          }
        );
      }
    );
  }

  ngOnDestroy(): void {
    this.removeBtnSubscription.unsubscribe();
    this.editBtnSubscription.unsubscribe();
    if (this.timerSubscription && this.timerSubscription instanceof Subscription) {
      this.timerSubscription.unsubscribe();
    }
  }
}
