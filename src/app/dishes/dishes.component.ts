import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material';
import {Router} from '@angular/router';
import {DishService} from '../service/dish.service';
import {Subscription} from 'rxjs';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-dishes',
  templateUrl: './dishes.component.html',
  styleUrls: ['./dishes.component.scss']
})
export class DishesComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('sidenav', {static: false}) sidenav: MatSidenav;
  editableDish: boolean;
  newDish: boolean;
  ingredientFormIsValid: boolean;
  private formChangesSubscription: Subscription;
  private editBtnSubscription: Subscription;
  private form: NgForm;

  constructor(private router: Router,
              private dishService: DishService) { }

  ngOnInit() {
    this.editableDish = false;
    this.editBtnSubscription = this.dishService.editButtonPressed.subscribe(
      (editButtonPressed: boolean) => this.editableDish = editButtonPressed
    );
    this.newDish = false;
    this.ingredientFormIsValid = false;
    this.formChangesSubscription = this.dishService.ingredientFormValid.subscribe(
      form => {
        this.form = form;
        this.ingredientFormIsValid = form.valid;
      }
    );
  }

  goToDishes() {
    if (this.editableDish) {
      this.changeFormToNotEditable();
    }
    this.newDish = false;
    this.router.navigate(['/dishes']);
    this.sidenav.close();
  }

  saveChangesInDish() {
    this.sidenav.close();
    this.router.navigate(['/dishes']);
    this.form.ngSubmit.emit();
  }

  removeDish() {
    this.dishService.removeButtonPressed.next();
    this.sidenav.close();
    this.router.navigate(['/dishes']);
  }

  changeFormToEditable() {
    this.editableDish = true;
    this.dishService.editButtonPressed.next(this.editableDish);
  }

  changeFormToNotEditable() {
    this.editableDish = false;
    this.dishService.editButtonPressed.next(this.editableDish);
  }

  addNewDish() {
    this.sidenav.open();
    this.newDish = true;
    this.changeFormToEditable();
  }

  ngAfterViewInit(): void {
    if (!this.sidenav.opened) {
      this.router.navigate(['/dishes']);
      this.dishService.editButtonPressed.next(false);
    }
  }

  ngOnDestroy(): void {
    this.formChangesSubscription.unsubscribe();
    this.editBtnSubscription.unsubscribe();
  }
}
