import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Dish} from '../../model/dish/dish.model';
import {DishService} from '../../service/dish.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-dishes-list',
  templateUrl: './dishes-list.component.html',
  styleUrls: ['./dishes-list.component.scss']
})
export class DishesListComponent implements OnInit, OnDestroy {
  dishes: Dish[];
  @Input() sidenav: ViewChild;
  editBtnSubscription: Subscription;
  editableDish: boolean;
  private dishesSubscription: Subscription;

  constructor(private dishService: DishService) { }

  ngOnInit() {
    this.dishService.retrieveDishes().subscribe(
      (dishes: Dish[]) => {
        this.dishService.saveDishes(dishes);
        this.dishes = dishes;
      }
    );

    this.dishesSubscription = this.dishService.dishesChanged.subscribe(
      dishes => {
        this.dishes = dishes;
        console.log('lista dan sie zmienila');
      }
    );

    this.editBtnSubscription = this.dishService.editButtonPressed.subscribe(
      (editButtonPressed: boolean) => {
        this.editableDish = editButtonPressed;
      }
    );
  }

  ngOnDestroy(): void {
    this.editBtnSubscription.unsubscribe();
    this.dishesSubscription.unsubscribe();
  }

}
