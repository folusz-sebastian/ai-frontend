import {Component, Input, OnInit} from '@angular/core';
import {Dish} from '../../../model/dish/dish.model';

@Component({
  selector: 'app-dish',
  templateUrl: './dish.component.html',
  styleUrls: ['./dish.component.scss']
})
export class DishComponent implements OnInit {
  @Input() dish: Dish;

  constructor() { }

  ngOnInit() {
  }

  ingredientsList(): string {
    if (this.dish.dishIngredients) {
      return this.dish.dishIngredients.map(e => e.ingredient.name).join(', ');
    }
    return '';
  }

}
