import {RouterModule, Routes} from '@angular/router';
import {DishesComponent} from './dishes.component';
import {NgModule} from '@angular/core';
import {AuthGuardService} from '../service/auth-guard.service';
import {DishInfoComponent} from './dish-info/dish-info.component';
import {Role} from '../model/user/Role';

const routes: Routes = [
  {
    path: 'dishes', component: DishesComponent, canActivate: [AuthGuardService], data: { roles: [Role.Employee] }, children: [
      {path: 'newDish', component: DishInfoComponent, canActivate: [AuthGuardService], data: { roles: [Role.Employee] }},
      {path: ':id', component: DishInfoComponent, canActivate: [AuthGuardService], data: { roles: [Role.Employee] }},
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DishesRoutingModule {

}
