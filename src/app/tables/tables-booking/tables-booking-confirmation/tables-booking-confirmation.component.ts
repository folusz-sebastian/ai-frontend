import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Table} from '../../../model/restaurantTable/table.model';
import {Reservation} from '../../../model/reservation/reservation.model';
import {FreeTablesService} from '../../../service/free-tables.service';
import {StatusBarService} from '../../../service/status-bar.service';
import {Subscription} from 'rxjs';
import {JsonConvert, ValueCheckingMode} from 'json2typescript';

@Component({
  selector: 'app-tables-booking-confirmation',
  templateUrl: './tables-booking-confirmation.component.html',
  styleUrls: ['./tables-booking-confirmation.component.scss']
})
export class TablesBookingConfirmationComponent implements OnInit, OnDestroy {
  private static savingReservationMessage = 'Rezerwacja została zapisana';
  table: Table;
  tableIndex: number;
  reservation: Reservation;
  private timerSubscription: Subscription;
  private jsonConvert = new JsonConvert();

  constructor(public modal: NgbActiveModal,
              private freeTablesService: FreeTablesService,
              private statusBarService: StatusBarService) {
    this.jsonConvert.ignorePrimitiveChecks = false;
    this.jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL;
  }

  ngOnInit() {
    this.table = this.freeTablesService.getTable(this.tableIndex);
  }

  bookTable() {
    this.reservation.table = this.table;
    this.reservation.beginningHour = +this.reservation.beginningHour;
    this.reservation.duration = +this.reservation.duration;
    this.freeTablesService.saveReservation(this.reservation).subscribe(
      (reservation: Reservation) => {
        console.log(reservation);
        this.freeTablesService.tableReservationChanged.next(true);
        const deserializedReservation = this.jsonConvert.deserializeObject(reservation, Reservation);
        localStorage.setItem('reservation', JSON.stringify(deserializedReservation));
        this.timerSubscription =
          this.statusBarService.temporarilyShowStatusBar(TablesBookingConfirmationComponent.savingReservationMessage);
        this.freeTablesService.removeTable(this.tableIndex);
      }
    );
    this.modal.close('Ok click');
  }

  ngOnDestroy(): void {
    if (this.timerSubscription && this.timerSubscription instanceof Subscription) {
      this.timerSubscription.unsubscribe();
    }
  }
}
