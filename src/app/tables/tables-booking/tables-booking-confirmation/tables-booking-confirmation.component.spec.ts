import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablesBookingConfirmationComponent } from './tables-booking-confirmation.component';

describe('TablesBookingConfirmationComponent', () => {
  let component: TablesBookingConfirmationComponent;
  let fixture: ComponentFixture<TablesBookingConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablesBookingConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablesBookingConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
