import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablesBookingComponent } from './tables-booking.component';

describe('TablesBookingComponent', () => {
  let component: TablesBookingComponent;
  let fixture: ComponentFixture<TablesBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablesBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablesBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
