import {AfterViewInit, Component, ElementRef, NgZone, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Table} from '../../model/restaurantTable/table.model';
import {UtilsService} from '../../service/utils.service';
import {NgbCalendar, NgbDatepickerConfig, NgbDateStruct, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AppSettings} from '../../app-settings';
import {Reservation} from '../../model/reservation/reservation.model';
import {FreeTablesService} from '../../service/free-tables.service';
import {TablesBookingConfirmationComponent} from './tables-booking-confirmation/tables-booking-confirmation.component';
import {StatusBarService} from '../../service/status-bar.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {GoogleCalendarService} from '../../service/google-calendar.service';
import {ResponseMessage} from '../../model/ResponseMessage.model';

@Component({
  selector: 'app-tables-booking',
  templateUrl: './tables-booking.component.html',
  styleUrls: ['./tables-booking.component.scss']
})
export class TablesBookingComponent implements OnInit, AfterViewInit, OnDestroy {
  private static retrieveTablesMessage = 'stoliki zostały zaktualizowane';
  @ViewChild('restaurantBounds', {static: false}) restaurantBounds: ElementRef;
  @ViewChild('datepicker', {static: false}) datePicker;
  tables: Table[];
  restaurantWorkingHours: number[];
  bookingDurationsList: number[];
  reservation: Reservation;
  showAlertToSaveEventToGoogleCal: boolean;

  reservationDate: NgbDateStruct;
  private timerSubscription: Subscription;
  private tableReservationSubscription: Subscription;
  private timerSubscriptionForCalendarEventAdding: Subscription;

  constructor(private ngZone: NgZone,
              private renderer: Renderer2,
              private elRef: ElementRef,
              private tablesService: FreeTablesService,
              private datepickerConfig: NgbDatepickerConfig,
              private calendar: NgbCalendar,
              private modalService: NgbModal,
              private statusBarService: StatusBarService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private googleCalendarService: GoogleCalendarService) {
    datepickerConfig.minDate = calendar.getNext(calendar.getToday(), 'd', 1);
    datepickerConfig.maxDate = calendar.getNext(calendar.getToday(), 'd', 11);
    datepickerConfig.startDate = calendar.getNext(calendar.getToday(), 'd', 1);
  }

  ngOnInit() {
    this.showAlertToSaveEventToGoogleCal = false;
    this.reservation = new Reservation();
    this.reservationDate = this.calendar.getNext(this.calendar.getToday(), 'd', 1);
    this.reservation.date = UtilsService.dateToString(this.reservationDate);
    this.restaurantWorkingHours = UtilsService.restaurantWorkingHoursList();
    this.bookingDurationsList = UtilsService.createBookingDurationsList(AppSettings.RESTAURANT_STARTING_JOB_HOUR);
    console.log(this.reservationDate.year + ', ' + this.reservationDate.month + ', ' + this.reservationDate.day);

    this.retrieveTables();

    window.onresize = () => {
      this.ngZone.run(() => {
        const newWidth = (this.restaurantBounds.nativeElement as HTMLElement).offsetWidth;
        this.tablesService.restaurantWidth = newWidth;
        this.renderer.setStyle(
          this.elRef.nativeElement.querySelector('#restaurant-bounds'), 'height', newWidth + 'px'
        );
      });
    };

    this.activatedRoute.queryParams.subscribe(
      params => {
        const lastReservation = JSON.parse(localStorage.getItem('reservation'));

        if (params['code'] !== undefined && lastReservation !== undefined) {
          const startDateTime = UtilsService.toDateTimeString(
            lastReservation.date,
            lastReservation.beginningHour
          );
          const endDateTime = UtilsService.toDateTimeString(
            lastReservation.date,
            lastReservation.beginningHour + lastReservation.duration
          );
          const reservationTitle = 'Rezerwacja na stolik o numerze: ' + lastReservation.table.id;
          this.googleCalendarService.saveEventToGoogleCalendar(params['code'], startDateTime, endDateTime, reservationTitle).subscribe(
            (responseMessage: ResponseMessage) => {
              console.log(JSON.stringify(responseMessage));
              this.timerSubscriptionForCalendarEventAdding = this.statusBarService.temporarilyShowStatusBar(responseMessage.message);
              this.router.navigate(['tables/booking']);
            }
          );
        } else {
          localStorage.removeItem('reservation');
        }
      }
    );

    this.tableReservationSubscription = this.tablesService.tableReservationChanged.subscribe(
      (tableIsReserved: boolean) => this.showAlertToSaveEventToGoogleCal = tableIsReserved
    );
  }

  private retrieveTables() {
    this.tablesService.retrieveTables(
      this.reservationDate.year.toString(), UtilsService.toTwoDigitsNumber(this.reservationDate.month),
      UtilsService.toTwoDigitsNumber(this.reservationDate.day), this.reservation.beginningHour, this.reservation.duration).subscribe(
      (tables: Table[]) => {
        this.tables = this.tablesService.getTables(tables);
        console.log(this.tables);
        this.timerSubscription = this.statusBarService.temporarilyShowStatusBar(TablesBookingComponent.retrieveTablesMessage);
      }
    );
  }

  ngAfterViewInit(): void {
    this.tablesService.restaurantWidth = (this.restaurantBounds.nativeElement as HTMLElement).offsetWidth;
    this.renderer.setStyle(
      this.elRef.nativeElement
        .querySelector('#restaurant-bounds'), 'height', this.tablesService.restaurantWidth + 'px'
    );
  }

  percentsToPixels(percents: number): number {
    return UtilsService.percentsToPixels(percents, this.tablesService.restaurantWidth);
  }

  onPressTable(tableIndex: number) {
    console.log('stół id: ' + this.tables[tableIndex].id);
    const activeModal = this.modalService.open(TablesBookingConfirmationComponent);
    activeModal.componentInstance.reservation = this.reservation;
    activeModal.componentInstance.tableIndex = tableIndex;
  }

  onChangingBeginningHour() {
    console.log(this.reservation.beginningHour);
    this.bookingDurationsList = UtilsService.createBookingDurationsList(this.reservation.beginningHour);
  }

  filter() {
    this.reservation.date = UtilsService.dateToString(this.reservationDate);
    this.retrieveTables();
  }

  saveReservationToGCal() {
    this.googleCalendarService.loginToGoogle().subscribe(
      (responseMessage: ResponseMessage) => {
        const googleLoginUrl = responseMessage.message;
        console.log(googleLoginUrl);
        window.location.href = googleLoginUrl;
      }
    );
  }

  ngOnDestroy(): void {
    if (this.timerSubscription && this.timerSubscription instanceof Subscription) {
      this.timerSubscription.unsubscribe();
    }
    if (this.timerSubscriptionForCalendarEventAdding && this.timerSubscriptionForCalendarEventAdding instanceof Subscription) {
      this.timerSubscriptionForCalendarEventAdding.unsubscribe();
    }
    this.tableReservationSubscription.unsubscribe();
  }

}
