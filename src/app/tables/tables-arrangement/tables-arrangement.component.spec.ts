import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablesArrangementComponent } from './tables-arrangement.component';

describe('TablesArrangementComponent', () => {
  let component: TablesArrangementComponent;
  let fixture: ComponentFixture<TablesArrangementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablesArrangementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablesArrangementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
