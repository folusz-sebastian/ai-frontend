import {Component, Input, OnInit} from '@angular/core';
import {TableView} from '../../../model/restaurantTable/table-view.model';

@Component({
  selector: 'app-tables-list',
  templateUrl: './tables-list.component.html',
  styleUrls: ['./tables-list.component.scss']
})
export class TablesListComponent implements OnInit {
  @Input() tables: TableView[];

  constructor() { }

  ngOnInit() {
  }
}
