import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {TablesService} from '../../../../service/tables.service';
import {TableView} from '../../../../model/restaurantTable/table-view.model';
import {StatusBarService} from '../../../../service/status-bar.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnDestroy {
  private static deletingTableMessage = 'Stolik został usunięty';
  @Input() table: TableView;
  @Input() tableIndex: number;
  private timerSubscription: Subscription;

  constructor(private tablesArrangementService: TablesService,
              private statusBarService: StatusBarService) { }

  ngOnInit() {
  }

  onChangeTableWidth() {
    this.tablesArrangementService.onChangingTableSize.next();
  }

  onChangeTableLength() {
    this.tablesArrangementService.onChangingTableSize.next();
  }

  removeTable() {
    if (this.table.table.id == null) {
      this.tablesArrangementService.removeTableFromArray(this.tableIndex);
    } else {
      this.tablesArrangementService.removeTable(this.table.table.id).subscribe(
        (deletingMessage) => {
          console.log(deletingMessage);
          this.timerSubscription = this.statusBarService.temporarilyShowStatusBar(TableComponent.deletingTableMessage);
          this.tablesArrangementService.removeTableFromArray(this.tableIndex);
        }
      );
    }
  }

  ngOnDestroy(): void {
    if (this.timerSubscription && this.timerSubscription instanceof Subscription) {
      this.timerSubscription.unsubscribe();
    }
  }
}
