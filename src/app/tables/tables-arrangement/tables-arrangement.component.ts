import {AfterViewInit, Component, ElementRef, NgZone, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {TablesService} from '../../service/tables.service';
import {Subscription} from 'rxjs';
import {TableView} from '../../model/restaurantTable/table-view.model';
import {Table} from '../../model/restaurantTable/table.model';
import {UtilsService} from '../../service/utils.service';
import {StatusBarService} from '../../service/status-bar.service';

@Component({
  selector: 'app-tables-arrangement',
  templateUrl: './tables-arrangement.component.html',
  styleUrls: ['./tables-arrangement.component.scss']
})
export class TablesArrangementComponent implements OnInit, AfterViewInit, OnDestroy {
  private static savingTablesArrangementMessage = 'Konfiguracja stolików została zapisana';
  @ViewChild('restaurantBounds', {static: false}) restaurantBounds: ElementRef;
  tables: TableView[] = [];
  private onChangingTableSizeSubscription: Subscription;
  private intersectedTablesNumbersSet: Set<number> = new Set();
  private timerSubscription: Subscription;

  constructor(private ngZone: NgZone,
              private renderer: Renderer2,
              private elRef: ElementRef,
              private tablesService: TablesService,
              private statusBarService: StatusBarService) {}

  ngOnInit() {
    this.tablesService.retrieveTables().subscribe(
      (tables: Table[]) => {
        this.tables = this.tablesService.getTables(tables);
      }
    );

    window.onresize = () => {
      this.ngZone.run(() => {
        const newWidth = (this.restaurantBounds.nativeElement as HTMLElement).offsetWidth;
        this.tablesService.restaurantWidth = newWidth;
        this.renderer.setStyle(
          this.elRef.nativeElement.querySelector('#restaurant-bounds'), 'height', newWidth + 'px'
        );
      });
    };
    this.onChangingTableSizeSubscription = this.tablesService.onChangingTableSize.subscribe(
      () => {
        this.checkTablesIntersections();
      }
    );
  }

  ngAfterViewInit(): void {
    this.tablesService.restaurantWidth = (this.restaurantBounds.nativeElement as HTMLElement).offsetWidth;
    this.renderer.setStyle(
      this.elRef.nativeElement
        .querySelector('#restaurant-bounds'), 'height', this.tablesService.restaurantWidth + 'px'
    );
  }

  addTable() {
    this.tablesService.addTable();
    this.checkTablesIntersections();
  }

  saveTablesArrangement() {
    if (this.intersectedTablesNumbersSet.size === 0) {
      this.tablesService.saveTablesArrangement().subscribe(
        (returnedTables: Table[]) => {
          console.log(returnedTables);
          this.timerSubscription =
            this.statusBarService.temporarilyShowStatusBar(TablesArrangementComponent.savingTablesArrangementMessage);
        }
      );
    }
  }

  onMoveEnd(index: number, event) {
    this.tables[index].table.position.xInPercents = Math.round(this.pixelsToPercents(event.x) * 100) / 100;
    this.tables[index].table.position.yInPercents = Math.round(this.pixelsToPercents(event.y) * 100) / 100;
    this.checkTablesIntersections();
  }

  private intersectRect(table1: TableView, table2: TableView) {
    return !(
      this.percentsToPixels(table2.table.position.xInPercents) >=
      (this.percentsToPixels(table1.table.position.xInPercents) + this.percentsToPixels(table1.table.widthInPercents)) ||
      (this.percentsToPixels(table2.table.position.xInPercents) + this.percentsToPixels(table2.table.widthInPercents)) <=
      this.percentsToPixels(table1.table.position.xInPercents) ||
      this.percentsToPixels(table2.table.position.yInPercents) >=
      (this.percentsToPixels(table1.table.position.yInPercents) + this.percentsToPixels(table1.table.lengthInPercents)) ||
      (this.percentsToPixels(table2.table.position.yInPercents) + this.percentsToPixels(table2.table.lengthInPercents)) <=
      this.percentsToPixels(table1.table.position.yInPercents)
    );
  }

  private checkTablesIntersections() {
    this.intersectedTablesNumbersSet.clear();
    for (let j = 0; j < this.tables.length; j++) {
      for (let i = 0; i < this.tables.length; i++) {
        if (i !== j) {
          if (this.intersectRect(this.tables[j], this.tables[i]) === true) {
            this.intersectedTablesNumbersSet.add(i);
          }
        }
      }
      if (this.tablesService.tableOutOfBounds(j)) {
        this.intersectedTablesNumbersSet.add(j);
      }
    }
    for (let i = 0; i < this.tables.length; i++) {
      this.tables[i].isCollision = this.intersectedTablesNumbersSet.has(i);
    }
  }

  percentsToPixels(percents: number): number {
    return UtilsService.percentsToPixels(percents, this.tablesService.restaurantWidth);
  }

  pixelsToPercents(pixels: number): number {
    return UtilsService.pixelsToPercents(pixels, this.tablesService.restaurantWidth);
  }

  ngOnDestroy(): void {
    this.onChangingTableSizeSubscription.unsubscribe();
    if (this.timerSubscription && this.timerSubscription instanceof Subscription) {
      this.timerSubscription.unsubscribe();
    }
  }
}
