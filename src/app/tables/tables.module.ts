import {NgModule} from '@angular/core';
import {TablesArrangementComponent} from './tables-arrangement/tables-arrangement.component';
import {TablesListComponent} from './tables-arrangement/tables-list/tables-list.component';
import {TableComponent} from './tables-arrangement/tables-list/table/table.component';
import {TablesComponent} from './tables.component';
import {TablesRoutingModule} from './tables-routing.module';
import {CommonModule} from '@angular/common';
import {TablesService} from '../service/tables.service';
import { TablesBookingComponent } from './tables-booking/tables-booking.component';
import {AngularDraggableModule} from 'angular2-draggable';
import {MatSliderModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FreeTablesService} from '../service/free-tables.service';
import { TablesBookingConfirmationComponent } from './tables-booking/tables-booking-confirmation/tables-booking-confirmation.component';
import {GoogleCalendarService} from '../service/google-calendar.service';

@NgModule({
  declarations: [
    TablesComponent,
    TablesArrangementComponent,
    TablesListComponent,
    TableComponent,
    TablesBookingComponent,
    TablesBookingConfirmationComponent
  ],
  imports: [
    CommonModule,
    TablesRoutingModule,
    AngularDraggableModule,
    MatSliderModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule
  ],
  exports: [],
  providers: [
    TablesService,
    FreeTablesService,
    GoogleCalendarService
  ],
  entryComponents: [TablesBookingConfirmationComponent],
})

export class TablesModule {

}
