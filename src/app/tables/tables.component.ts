import {AfterViewInit, Component, ElementRef, NgZone, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {TableView} from '../model/restaurantTable/table-view.model';
import {Subscription} from 'rxjs';
import {TablesService} from '../service/tables.service';
import {Table} from '../model/restaurantTable/table.model';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss']
})
export class TablesComponent {

}
