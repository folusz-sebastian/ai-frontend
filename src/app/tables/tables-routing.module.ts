import {RouterModule, Routes} from '@angular/router';
import {AuthGuardService} from '../service/auth-guard.service';
import {NgModule} from '@angular/core';
import {TablesComponent} from './tables.component';
import {TablesArrangementComponent} from './tables-arrangement/tables-arrangement.component';
import {TablesBookingComponent} from './tables-booking/tables-booking.component';
import {Role} from '../model/user/Role';

const routes: Routes = [
  {
    path: 'tables', component: TablesComponent, canActivate: [AuthGuardService], children: [
      {path: 'booking', component: TablesBookingComponent, canActivate: [AuthGuardService], data: { roles: [Role.Customer] }},
      {path: 'arrangement', component: TablesArrangementComponent, canActivate: [AuthGuardService], data: { roles: [Role.Employee] }},
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TablesRoutingModule {

}
