import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {IngredientsComponent} from './ingredients.component';
import {AuthGuardService} from '../service/auth-guard.service';
import {Role} from '../model/user/Role';

const ingredientRoutes: Routes = [
  {
    path: 'ingredients', component: IngredientsComponent, canActivate: [AuthGuardService], data: { roles: [Role.Employee] }
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(ingredientRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class IngredientsRoutingModule {
}
