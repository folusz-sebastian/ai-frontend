import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Ingredient} from '../../model/dish/ingredient.model';
import {Subscription} from 'rxjs';
import {NgForm} from '@angular/forms';
import {JsonConvert, ValueCheckingMode} from 'json2typescript';
import {MeasurementUnit} from '../../model/dish/Measurement-unit.model';
import {IngredientService} from '../../service/ingredient.service';
import {StatusBarService} from '../../service/status-bar.service';

@Component({
  selector: 'app-ingredient-info',
  templateUrl: './ingredient-info.component.html',
  styleUrls: ['./ingredient-info.component.scss']
})
export class IngredientInfoComponent implements OnInit, AfterViewInit, OnDestroy {
  private static postIngredientStatusBarMessage = 'Składnik został dodany';
  @ViewChild('form', {static: false}) ingredientForm: NgForm;
  formChangesSubscription: Subscription;
  @Input() ingredient: Ingredient;
  measurementUnits: MeasurementUnit[];
  private timerSubscription: Subscription;

  constructor(private ingredientService: IngredientService,
              private statusBarService: StatusBarService) {}

  ngOnInit() {
    this.ingredientService.retrieveMeasurementUnits().subscribe(
      (measurementUnits: MeasurementUnit[]) => this.measurementUnits = measurementUnits
    );
  }

  saveForm() {
    this.ingredientService.postNewIngredient(this.ingredient).subscribe(
      (ingredient) => {
        this.timerSubscription = this.statusBarService.temporarilyShowStatusBar(IngredientInfoComponent.postIngredientStatusBarMessage);
        this.ingredientService.addNewIngredient(ingredient);
      }
    );
  }

  ngAfterViewInit() {
    this.formChangesSubscription = this.ingredientForm.form.valueChanges.subscribe(
      () => {
        this.ingredientService.ingredientFormValid.next(this.ingredientForm);
      }
    );
  }

  ngOnDestroy(): void {
    this.formChangesSubscription.unsubscribe();
    if (this.timerSubscription && this.timerSubscription instanceof Subscription) {
      this.timerSubscription.unsubscribe();
    }
  }
}
