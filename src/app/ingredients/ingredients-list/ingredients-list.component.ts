import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Ingredient} from '../../model/dish/ingredient.model';
import {IngredientService} from '../../service/ingredient.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-ingredients-list',
  templateUrl: './ingredients-list.component.html',
  styleUrls: ['./ingredients-list.component.scss']
})
export class IngredientsListComponent implements OnInit {
  ingredients: Ingredient[] = [];
  @Input() sidenav: ViewChild;
  // private ingredientsSubscription: Subscription;

  constructor(private ingredientService: IngredientService) { }

  ngOnInit() {
    this.ingredientService.retrieveIngredients().subscribe(
      (ingredients: Ingredient[]) => {
        console.log('ingredients: ' + ingredients);
        this.ingredientService.saveIngredients(ingredients);
        this.ingredients = ingredients;
      }
    );
    // this.ingredientsSubscription = this.ingredientService.ingredientsChanged.subscribe(
    //   (ingredients: Ingredient[]) => {
    //     this.ingredients = ingredients;
    //   }
    // );
  }

}
