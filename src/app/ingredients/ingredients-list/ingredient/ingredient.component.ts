import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Ingredient} from '../../../model/dish/ingredient.model';
import {IngredientService} from '../../../service/ingredient.service';
import {StatusBarService} from '../../../service/status-bar.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss']
})
export class IngredientComponent implements OnInit, OnDestroy {
  private static deleteIngredientStatusBarMessage = 'Składnik został usunięty';
  @Input() ingredient: Ingredient;
  @Input() ingredientIndex: number;
  private timerSubscription: Subscription;

  constructor(private ingredientService: IngredientService,
              private statusBarService: StatusBarService) { }

  ngOnInit() {
  }

  removeIngredient() {
    this.ingredientService.deleteIngredient(this.ingredientIndex).subscribe(
      (response) => {
        console.log(response);
        this.timerSubscription = this.statusBarService.temporarilyShowStatusBar(IngredientComponent.deleteIngredientStatusBarMessage);
      }
    );
  }

  ngOnDestroy(): void {
    if (this.timerSubscription && this.timerSubscription instanceof Subscription) {
      this.timerSubscription.unsubscribe();
    }
  }
}
