import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material';
import {Ingredient} from '../model/dish/ingredient.model';
import {Subscription} from 'rxjs';
import {NgForm} from '@angular/forms';
import {IngredientService} from '../service/ingredient.service';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.scss']
})
export class IngredientsComponent implements OnInit, OnDestroy {
  @ViewChild('sidenav', {static: false}) sidenav: MatSidenav;
  ingredientFormIsValid: boolean;
  ingredient: Ingredient;
  private formChangesSubscription: Subscription;
  private ingredientForm: NgForm;

  constructor(private ingredientService: IngredientService) { }

  ngOnInit() {
    this.ingredient = new Ingredient();
    this.ingredientFormIsValid = false;
    this.formChangesSubscription = this.ingredientService.ingredientFormValid.subscribe(
      ingredientForm => {
        this.ingredientForm = ingredientForm;
        this.ingredientFormIsValid = ingredientForm.valid;
      }
    );
  }

  saveNewIngredient() {
    this.ingredientForm.ngSubmit.emit();
    this.sidenav.close();
  }

  showForm() {
    this.ingredient = new Ingredient();
    this.sidenav.open();
  }

  ngOnDestroy(): void {
    this.formChangesSubscription.unsubscribe();
  }
}
