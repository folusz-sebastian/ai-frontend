import {IngredientService} from '../service/ingredient.service';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {IngredientsComponent} from './ingredients.component';
import {NgModule} from '@angular/core';
import {IngredientsRoutingModule} from './ingredients-routing.module';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {MatSidenavModule} from '@angular/material';
import { IngredientInfoComponent } from './ingredient-info/ingredient-info.component';
import { IngredientsListComponent } from './ingredients-list/ingredients-list.component';
import { IngredientComponent } from './ingredients-list/ingredient/ingredient.component';

@NgModule(
  {
    declarations: [
      IngredientsComponent,
      IngredientInfoComponent,
      IngredientsListComponent,
      IngredientComponent
    ],
    imports: [
      CommonModule,
      FormsModule,
      MatSidenavModule,
      IngredientsRoutingModule,
      CurrencyMaskModule
    ],
    providers: [
      IngredientService
    ]
  }
)

export class IngredientsModule {
}
