import { Injectable } from '@angular/core';
import {Ingredient} from '../model/dish/ingredient.model';
import {DataStorageService} from './data-storage.service';
import {Subject} from 'rxjs';
import {NgForm} from '@angular/forms';
import {JsonConvert, ValueCheckingMode} from 'json2typescript';

@Injectable()
export class IngredientService {
  ingredients: Ingredient[];
  ingredientFormValid = new Subject<NgForm>();
  private jsonConvert = new JsonConvert();

  constructor(private dataStorageService: DataStorageService) {
    this.jsonConvert.ignorePrimitiveChecks = false;
    this.jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL;
  }

  retrieveIngredients() {
    return this.dataStorageService.getIngredients();
  }

  retrieveMeasurementUnits() {
    return this.dataStorageService.getMeasurementUnits();
  }

  saveIngredients(ingredients: Ingredient[]) {
    this.ingredients = ingredients;
  }

  deleteIngredient(ingredientIndex: number) {
    const observable = this.dataStorageService.deleteIngredient(this.ingredients[ingredientIndex].id);
    this.ingredients.splice(ingredientIndex, 1);
    return observable;
  }

  postNewIngredient(newIngredient: Ingredient) {
    newIngredient.measurementUnit.id = +newIngredient.measurementUnit.id;
    return this.dataStorageService.postIngredient(newIngredient);
  }

  addNewIngredient(newIngredient: Ingredient) {
    const deserializedIngredient = this.jsonConvert.deserializeObject(newIngredient, Ingredient);
    this.ingredients.push(deserializedIngredient);
  }
}
