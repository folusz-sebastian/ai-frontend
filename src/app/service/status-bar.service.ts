import {Injectable} from '@angular/core';
import { Observable } from 'rxjs';
import { timer } from 'rxjs';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatusBarService {
  private static showingStatusBarDuration = 2000;
  private timer: Observable<any>;
  showStatusBarChanged = new Subject<boolean>();
  message: string;

  temporarilyShowStatusBar(message: string) {
    this.message = message;
    this.showStatusBarChanged.next(true);

    this.timer = timer(StatusBarService.showingStatusBarDuration);
    return this.timer.subscribe(() => {
      this.showStatusBarChanged.next(false);
    });
  }
}
