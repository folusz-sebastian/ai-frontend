import { Injectable } from '@angular/core';
import {AppSettings} from '../app-settings';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }

  static percentsToPixels(percents: number, fullSize: number): number {
    return percents / 100 * fullSize;
  }

  static pixelsToPercents(pixels: number, fullSize: number): number {
    return pixels / fullSize * 100;
  }

  static restaurantWorkingHoursList(): number[] {
    const workingHours: number[] = [];
    const workingHoursLength = AppSettings.RESTAURANT_ENDING_JOB_HOUR - AppSettings.RESTAURANT_STARTING_JOB_HOUR;
    let hour = AppSettings.RESTAURANT_STARTING_JOB_HOUR;
    for (let i = 0; i < workingHoursLength; i++) {
      workingHours.push(hour++);
    }
    return workingHours;
  }

  static createBookingDurationsList(choosedBookingHour: number) {
    const bookingDurationsList: number[] = [];
    for (let i = AppSettings.RESTAURANT_ENDING_JOB_HOUR - 1; i >= choosedBookingHour; i--) {
      bookingDurationsList.push(AppSettings.RESTAURANT_ENDING_JOB_HOUR - i);
    }
    return bookingDurationsList;
  }

  static dateToString(date: NgbDateStruct): string {
    return date.year + '-' +
      this.toTwoDigitsNumber(date.month) + '-' +
      this.toTwoDigitsNumber(date.day);
  }

  static toDateTimeString(date: string, hour: number) {
    return date + 'T' + this.toTwoDigitsNumber(hour) + ':00:00';
  }


  static toTwoDigitsNumber(number: number): string {
    return number < 10 ? '0' + number : number.toString();
  }
}
