import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {JsonConvert, ValueCheckingMode} from 'json2typescript';
import {AuthService} from './auth.service';
import {DishCategory} from '../model/dish/dish-category.model';
import {Ingredient} from '../model/dish/ingredient.model';
import {User} from '../model/user/User.model';
import {Dish} from '../model/dish/dish.model';
import {MeasurementUnit} from '../model/dish/Measurement-unit.model';
import {Table} from '../model/restaurantTable/table.model';
import {Reservation} from '../model/reservation/reservation.model';
import {Observable} from 'rxjs';
import {ResponseMessage} from '../model/ResponseMessage.model';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {
  private url = 'http://localhost:5050/';
  private userUrl = this.fullPathEndpoint('api/user');
  private dishesUrl = this.fullPathEndpoint('api/dishes');
  private dishCategoriesUrl = this.fullPathEndpoint('api/dishCategories');
  private ingredientsUrl = this.fullPathEndpoint('api/ingredients');
  private measurementUnitsUrl = this.fullPathEndpoint('api/measurementUnits');
  private tablesUrl = this.fullPathEndpoint('api/tables');
  private freeTablesUrl = this.fullPathEndpoint('api/free_tables');
  private reservationsUrl = this.fullPathEndpoint('api/reservations');
  private loginToGoogleUrl = this.fullPathEndpoint('api/login_to_google');
  private saveEventToGoogleCalUrl = this.fullPathEndpoint('api/calendar_event');
  private httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
  private jsonConvert = new JsonConvert();

  constructor(private httpClient: HttpClient) {
    this.jsonConvert.ignorePrimitiveChecks = false;
    this.jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL;
  }

  private fullPathEndpoint(theEndOfUrl: string): string {
    return this.url + theEndOfUrl;
  }

  getUser(encryptedUserData: string) {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', encryptedUserData);
    return this.httpClient.get<User>(this.userUrl, this.httpOptions);
  }

  getIngredients() {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());
    return this.httpClient.get<Ingredient[]>(this.ingredientsUrl, httpOptions)
      .pipe(
        map(
          (ingredients): Ingredient[] => {
            const deserializedIngredients = [];
            for (let ingredient of ingredients) {
              ingredient = this.jsonConvert.deserializeObject(ingredient, Ingredient);
              deserializedIngredients.push(ingredient);
            }
            return deserializedIngredients;
          }
        )
      );
  }

  getMeasurementUnits() {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());
    return this.httpClient.get<MeasurementUnit[]>(this.measurementUnitsUrl, httpOptions)
      .pipe(
        map(
          (measurementUnits): MeasurementUnit[] => {
            const deserializedMeasurementUnits = [];
            for (let measurementUnit of measurementUnits) {
              measurementUnit = this.jsonConvert.deserializeObject(measurementUnit, MeasurementUnit);
              deserializedMeasurementUnits.push(measurementUnit);
            }
            return deserializedMeasurementUnits;
          }
        )
      );
  }

  deleteIngredient(ingredientId: number) {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());

    return this.httpClient.delete<Ingredient>(this.ingredientsUrl + '/' + ingredientId, httpOptions);
  }

  postIngredient(ingredient: Ingredient) {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());

    const serializedIngredient = this.jsonConvert.serializeObject(ingredient);
    return this.httpClient.post<Ingredient>(this.ingredientsUrl, serializedIngredient, httpOptions);
  }

  getDishes() {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());
    return this.httpClient.get<Dish[]>(this.dishesUrl, httpOptions)
      .pipe(
        map(
          (dishes): Dish[] => {
            const deserializedDishes = [];
            for (let dish of dishes) {
              dish = this.jsonConvert.deserializeObject(dish, Dish);
              deserializedDishes.push(dish);
            }
            return deserializedDishes;
          }
        )
      );
  }

  getDishCategories() {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());
    return this.httpClient.get<DishCategory[]>(this.dishCategoriesUrl, httpOptions)
      .pipe(
        map(
          (dishCategories): DishCategory[] => {
            const deserializedDishCategories = [];
            for (let dishCategory of dishCategories) {
              dishCategory = this.jsonConvert.deserializeObject(dishCategory, DishCategory);
              deserializedDishCategories.push(dishCategory);
            }
            return deserializedDishCategories;
          }
        )
      );

  }

  postDish(dish: Dish) {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());

    const serializedDish = this.jsonConvert.serializeObject(dish);
    console.log('posting dish: ' + serializedDish);
    return this.httpClient.post<Dish>(this.dishesUrl, serializedDish, httpOptions);
  }

  putDish(dish: Dish) {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());

    const serializedDish = this.jsonConvert.serializeObject(dish);
    return this.httpClient.put<Dish>(this.dishesUrl + '/' + dish.id, serializedDish, httpOptions);
  }

  deleteDish(dishId: number) {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());

    return this.httpClient.delete<Dish>(this.dishesUrl + '/' + dishId, httpOptions);
  }

  private getTables(url: string) {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());
    return this.httpClient.get<Table[]>(url, httpOptions)
      .pipe(
        map(
          (tables): Table[] => {
            const deserializedTables: Table[] = [];
            for (let table of tables) {
              table = this.jsonConvert.deserializeObject(table, Table);
              deserializedTables.push(table);
            }
            return deserializedTables;
          }
        )
      );
  }

  getFreeTables(year: string, month: string, day: string, beginningHour: number, duration: number) {
    const url = this.freeTablesUrl + '?year=' + year + '&month=' + month + '&day=' + day +
      '&beginning_hour=' + beginningHour + '&duration=' + duration;
    return this.getTables(url);
  }

  getAllTables() {
    return this.getTables(this.tablesUrl);
  }

  putTables(tables: Table[]): Observable<Table[]> {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());

    const serializedTables = this.jsonConvert.serializeArray(tables);
    return this.httpClient.put<Table[]>(this.tablesUrl , serializedTables, httpOptions);
  }

  deleteTable(tableId: number) {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());

    return this.httpClient.delete<Table>(this.tablesUrl + '/' + tableId, httpOptions);
  }

  postReservation(reservation: Reservation) {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());

    const serializedReservation = this.jsonConvert.serializeObject(reservation);
    return this.httpClient.post<Reservation>(this.reservationsUrl, serializedReservation, httpOptions);
  }

  loginToGoogle(): Observable<ResponseMessage> {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());
    return this.httpClient.get<ResponseMessage>(this.loginToGoogleUrl, httpOptions);
  }

  saveEventToGoogleCalendar(googleCalCode: string, startDateTime: string, endDateTime: string, title: string): Observable<ResponseMessage> {
    const httpOptions = {headers: new HttpHeaders().set('Content-Type', 'application/json')};
    httpOptions.headers = httpOptions.headers.set('Authorization', AuthService.getAuthorization());
    const url = this.saveEventToGoogleCalUrl +
      '?google_cal_code=' + googleCalCode +
      '&start_date_time=' +  startDateTime +
      '&end_date_time=' + endDateTime +
      '&title=' + title;
    return this.httpClient.get<ResponseMessage>(url, httpOptions);
  }
}
