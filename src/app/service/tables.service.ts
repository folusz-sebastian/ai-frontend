import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {TableView} from '../model/restaurantTable/table-view.model';
import {DataStorageService} from './data-storage.service';
import {Table} from '../model/restaurantTable/table.model';

@Injectable()
export class TablesService {
  tables: TableView[] = [];
  private _restaurantWidth: number;
  onChangingTableSize = new Subject();

  constructor(private dataStorageService: DataStorageService) { }

  get restaurantWidth(): number {
    return this._restaurantWidth;
  }

  set restaurantWidth(value: number) {
    this._restaurantWidth = value;
  }

  addTable() {
    this.tables.push(new TableView(new Table()));
  }

  saveTablesArrangement(): Observable<Table[]> {
    const tables: Table[] = this.tables.map(tableView => tableView.table);
    return this.dataStorageService.putTables(tables);
  }

  tableOutOfBounds(tableIndex: number): boolean {
    const table = this.tables[tableIndex];
    return this.percentsToPixels(table.table.position.xInPercents) +
      this.percentsToPixels(table.table.widthInPercents) > this.restaurantWidth ||
      this.percentsToPixels(table.table.position.yInPercents) + this.percentsToPixels(table.table.lengthInPercents) > this.restaurantWidth;
  }

  percentsToPixels(percents: number): number {
    return percents / 100 * this.restaurantWidth;
  }

  retrieveTables(): Observable<Table[]> {
    return this.dataStorageService.getAllTables();
  }

  extendTableToTableView(tables: Table[]): TableView[] {
    const tableViews: TableView[] = [];
    tables.forEach((table: Table) => tableViews.push(new TableView(table)));
    return tableViews;
  }

  getTables(tables: Table[]): TableView[] {
    this.tables = this.extendTableToTableView(tables);
    return this.tables;
  }

  removeTable(tableId: number) {
      return this.dataStorageService.deleteTable(tableId);
  }

  removeTableFromArray(tableIndex: number) {
    this.tables.splice(tableIndex, 1);
  }
}
