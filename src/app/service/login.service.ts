import { Injectable } from '@angular/core';
import {DataStorageService} from './data-storage.service';
import {AuthService} from './auth.service';

@Injectable()
export class LoginService {

  constructor(private dataStorageService: DataStorageService) { }

  authenticate(email: string, password: string) {
    return this.dataStorageService.getUser(
      AuthService.encrypt(email, password)
    );
  }
}
