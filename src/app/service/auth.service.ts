import { Injectable } from '@angular/core';
import {User} from '../model/user/User.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() {}

  static getAuthorization() {
    return localStorage.getItem('authorization');
  }

  static encrypt(email: string, password: string) {
    return 'Basic ' + btoa(email + ':' + password);
  }

  static saveUserLocally(user: User) {
    localStorage.setItem('user', JSON.stringify(user));
    console.log(localStorage.getItem('user'));
  }

  static saveAuthorization(email: string, password: string) {
    localStorage.setItem('authorization', AuthService.encrypt(email, password));
  }

  static logoutUser() {
    localStorage.removeItem('user');
    localStorage.removeItem('authorization');
  }

  private static loggedInUser(): User {
    return JSON.parse(localStorage.getItem('user'));
  }

  roleAuthorised(requiredRoles: string[]): boolean {
    const currentUserRoles: string[] = AuthService.loggedInUser().roles.map(userRole => userRole.role);
    for (let i = 0; i < currentUserRoles.length; i++) {
      for (let j = 0; j < requiredRoles.length; j++) {
        if (currentUserRoles[i] === requiredRoles[j]) {
          return true;
        }
      }
    }
    return false;
  }

  userIsLoggedIn() {
    return localStorage.getItem('user') !== null;
  }
}
