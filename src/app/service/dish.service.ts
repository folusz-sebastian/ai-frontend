import {Injectable} from '@angular/core';
import {DataStorageService} from './data-storage.service';
import {Dish} from '../model/dish/dish.model';
import {Subject} from 'rxjs';
import {NgForm} from '@angular/forms';
import {JsonConvert, ValueCheckingMode} from 'json2typescript';

@Injectable()
export class DishService {
  ingredientFormValid = new Subject<NgForm>();
  editButtonPressed = new Subject<boolean>();
  removeButtonPressed = new Subject();
  private dishes: Dish[] = [];
  dishesChanged = new Subject<Dish[]>();
  private jsonConvert = new JsonConvert();

  constructor(private dataStorageService: DataStorageService) {
    this.jsonConvert.ignorePrimitiveChecks = false;
    this.jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL;
  }

  retrieveDishes() {
    return this.dataStorageService.getDishes();
  }

  retrieveDishCategories() {
    return this.dataStorageService.getDishCategories();
  }

  retrieveIngredients() {
    return this.dataStorageService.getIngredients();
  }

  saveDishes(dishes: Dish[]) {
    this.dishes = dishes;
  }

  getDishes(): Dish[] {
    return this.dishes;
  }

  getDish(index: number) {
    return this.dishes[index];
  }

  removeDish(index: number) {
    const dishObservable = this.dataStorageService.deleteDish(this.dishes[index].id);
    this.dishes.splice(index, 1);
    return dishObservable;
  }

  postNewDish(dish: Dish) {
    dish.id = 0;
    return this.dataStorageService.postDish(dish);
  }

  saveNewDish(dish: Dish) {
    const deserializedDish = this.jsonConvert.deserializeObject(dish, Dish);
    this.dishes.push(deserializedDish);
  }

  updateDish(dishesArrayIndex: number, dish: Dish) {
    this.dishes[dishesArrayIndex] = this.jsonConvert.deserializeObject(dish, Dish);
    if (this.dishes[dishesArrayIndex].dishIngredients) {
      this.dishes[dishesArrayIndex].dishIngredients.forEach(
        dishIngredient => console.log('Mskladnik: ' + JSON.stringify(dishIngredient.ingredient))
      );
    }
    this.dishesChanged.next(this.dishes);
  }

  putDish(dish: Dish) {
    return this.dataStorageService.putDish(dish);
  }
}
