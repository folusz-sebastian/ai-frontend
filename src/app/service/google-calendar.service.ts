import { Injectable } from '@angular/core';
import {DataStorageService} from './data-storage.service';
import {Observable} from 'rxjs';
import {ResponseMessage} from '../model/ResponseMessage.model';

@Injectable()
export class GoogleCalendarService {

  constructor(private dataStorageService: DataStorageService) { }

  saveEventToGoogleCalendar(googleCode: string, startDateTime: string, endDateTime: string, title: string): Observable<ResponseMessage> {
    return this.dataStorageService.saveEventToGoogleCalendar(googleCode, startDateTime, endDateTime, title);
  }

  loginToGoogle(): Observable<ResponseMessage> {
    return this.dataStorageService.loginToGoogle();
  }
}
