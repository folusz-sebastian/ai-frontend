import { Injectable } from '@angular/core';
import {Table} from '../model/restaurantTable/table.model';
import {DataStorageService} from './data-storage.service';
import {Observable, Subject} from 'rxjs';
import {Reservation} from '../model/reservation/reservation.model';

@Injectable()
export class FreeTablesService {
  tables: Table[];
  private _restaurantWidth: number;
  tableReservationChanged = new Subject<boolean>();

  constructor(
    private dataStorageService: DataStorageService
  ) {}

  get restaurantWidth(): number {
    return this._restaurantWidth;
  }

  set restaurantWidth(value: number) {
    this._restaurantWidth = value;
  }

  retrieveTables(year: string, month: string, day: string, beginningHour: number, duration: number): Observable<Table[]> {
    return this.dataStorageService.getFreeTables(year, month, day, beginningHour, duration);
  }

  getTables(tables: Table[]): Table[] {
    this.tables = tables;
    return this.tables;
  }

  getTable(tableIndex: number): Table {
    return this.tables[tableIndex];
  }

  saveReservation(reservation: Reservation): Observable<Reservation> {
    return this.dataStorageService.postReservation(reservation);
  }

  removeTable(tableIndex: number) {
    this.tables.splice(tableIndex, 1);
  }
}
