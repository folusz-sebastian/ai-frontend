import {Component, OnInit} from '@angular/core';
import {LoginService} from '../service/login.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {AuthService} from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  errorOnLogin = false;

  constructor(private router: Router,
              private loginService: LoginService) { }

  ngOnInit() {
  }

  onLogin(form: NgForm) {
    this.loginService.authenticate(form.value.email, form.value.password).subscribe(
      user => {
        this.errorOnLogin = false;
        this.router.navigate(['']);
        AuthService.saveUserLocally(user);
        AuthService.saveAuthorization(form.value.email, form.value.password);
      }, () => {
        this.errorOnLogin = true;
        console.log('can\'t login');
      }
    );
  }

}
