import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../service/auth.service';
import {Role} from '../model/user/Role';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  employeeRoleName = Role.Employee;
  customerRoleName = Role.Customer;

  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    if (this.authService.userIsLoggedIn()) {
      console.log('userIsLoggedIn');
    }
  }

  logoutUser() {
    this.router.navigate(['login']);
    AuthService.logoutUser();
  }
}
