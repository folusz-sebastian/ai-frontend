export class AppSettings {
  public static RESTAURANT_STARTING_JOB_HOUR = 8;
  public static RESTAURANT_ENDING_JOB_HOUR = 20;
}
