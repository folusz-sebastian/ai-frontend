import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StatusBarComponent} from './status-bar.component';
import {StatusBarService} from '../service/status-bar.service';

@NgModule(
  {
    declarations: [
      StatusBarComponent
    ],
    imports: [
      CommonModule
    ],
    exports: [
      StatusBarComponent
    ],
    providers: [
      StatusBarService
    ]
  }
)

export class StatusBarModule {

}
