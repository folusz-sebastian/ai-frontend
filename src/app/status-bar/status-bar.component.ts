import {Component, OnDestroy, OnInit} from '@angular/core';
import {StatusBarService} from '../service/status-bar.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-status-bar',
  templateUrl: './status-bar.component.html',
  styleUrls: ['./status-bar.component.scss']
})
export class StatusBarComponent implements OnInit, OnDestroy {
  showStatusBar: boolean;
  subscription: Subscription;

  constructor(private statusBarService: StatusBarService) { }

  ngOnInit() {
    // this.showStatusBar = true;
    this.showStatusBar = false;
    this.subscription = this.statusBarService.showStatusBarChanged.subscribe(
      (showStatusBar: boolean) => this.showStatusBar = showStatusBar
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
