import {UserRole} from './UserRole.model';

export class User {
  constructor(
    public email: string,
    public name: string,
    public lastName: string,
    public roles: UserRole[]
  ) {}
}
