import {JsonObject, JsonProperty} from 'json2typescript';
import {Table} from '../restaurantTable/table.model';
import {AppSettings} from '../../app-settings';

@JsonObject('Reservation')
export class Reservation {
  @JsonProperty('id', Number)
  id: number;

  @JsonProperty('date', String)
  date: string;

  @JsonProperty('beginning_hour', Number)
  beginningHour: number;

  @JsonProperty('duration', Number)
  duration: number;

  @JsonProperty('table', Table)
  table: Table;

  constructor() {
    this.id = null;
    this.date = null;
    this.beginningHour = AppSettings.RESTAURANT_STARTING_JOB_HOUR;
    this.duration = 1;
    this.table = null;
  }
}
