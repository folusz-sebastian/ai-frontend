import {Position} from './position.model';
import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('Table')
export class Table {
  @JsonProperty('id', Number)
  public id: number;

  @JsonProperty('width_in_percents', Number)
  public widthInPercents: number;

  @JsonProperty('length_in_percents', Number)
  public lengthInPercents: number;

  @JsonProperty('position', Position)
  public position: Position;

  constructor() {
    this.id = null;
    this.widthInPercents = 20;
    this.lengthInPercents = 20;
    this.position = new Position();
  }
}
