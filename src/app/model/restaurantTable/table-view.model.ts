import {Table} from './table.model';

export class TableView {
  public table: Table;
  public isCollision: boolean;

  constructor(table: Table) {
    this.table = table;
    this.isCollision = false;
  }
}
