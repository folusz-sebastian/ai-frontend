import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('Position')
export class Position {
  @JsonProperty('id', Number)
  public id: number;

  @JsonProperty('x_in_percents', Number)
  public xInPercents: number;

  @JsonProperty('y_in_percents', Number)
  public yInPercents: number;

  constructor() {
    this.id = null;
    this.xInPercents = 0;
    this.yInPercents = 0;
  }
}
