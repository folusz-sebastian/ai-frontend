import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('MeasurementUnit')
export class MeasurementUnit {
  @JsonProperty('id', Number)
  id: number;

  @JsonProperty('name', String)
  name: string;

  constructor() {
    this.id = null;
    this.name = '';
  }
}
