import {JsonObject, JsonProperty} from 'json2typescript';
import {MeasurementUnit} from './Measurement-unit.model';

@JsonObject('Ingredient')
export class Ingredient {
  @JsonProperty('id', Number)
  id: number;

  @JsonProperty('name', String)
  name: string;

  @JsonProperty('measurement_unit', MeasurementUnit)
  measurementUnit: MeasurementUnit;

  constructor() {
    this.id = null;
    this.name = '';
    this.measurementUnit = new MeasurementUnit();
  }
}
