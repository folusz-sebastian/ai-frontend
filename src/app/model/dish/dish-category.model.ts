import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('DishCategory')
export class DishCategory {
  @JsonProperty('id', Number)
  id: number;

  @JsonProperty('name', String)
  name: string;

  constructor() {
    this.id = null;
    this.name = '';
  }
}
