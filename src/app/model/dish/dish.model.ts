import {JsonObject, JsonProperty} from 'json2typescript';
import {DishIngredient} from './dish-ingredient.model';
import {DishCategory} from './dish-category.model';

@JsonObject('Dish')
export class Dish {
  @JsonProperty('id', Number)
  id: number;

  @JsonProperty('name', String)
  name: string;

  @JsonProperty('price', Number)
  price: number;

  // @JsonProperty('image_url', String)
  // imageUrl: string;

  // @JsonProperty('description', String)
  // description: string;

  @JsonProperty('category', DishCategory)
  category: DishCategory;

  @JsonProperty('dish_ingredients', [DishIngredient])
  dishIngredients: DishIngredient[];

  constructor() {
    this.id = null;
    this.name = '';
    this.price = 0.0;
    // this.imageUrl = '';
    // this.description = '';
    this.category = new DishCategory();
    this.dishIngredients = [];
  }
}
