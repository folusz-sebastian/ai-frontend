import {JsonObject, JsonProperty} from 'json2typescript';
import {Ingredient} from './ingredient.model';

@JsonObject('DishIngredient')
export class DishIngredient {
  @JsonProperty('id', Number)
  id: number;

  @JsonProperty('ingredient', Ingredient)
  ingredient: Ingredient;

  @JsonProperty('quantity', Number)
  quantity: number;

  constructor() {
    this.id = null;
    this.ingredient = new Ingredient();
    this.quantity = 0.0;
  }
}
